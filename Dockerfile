FROM centos

RUN dnf update -y && dnf install httpd -y

RUN echo pruebas > /var/www/html/index.html

EXPOSE 80

ENTRYPOINT ["/usr/sbin/httpd", "-D", "FOREGROUND"]
